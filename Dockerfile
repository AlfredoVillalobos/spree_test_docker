# Install ruby
FROM ruby:2.6.3-stretch

# Set local timezone
RUN cp /usr/share/zoneinfo/America/Santiago /etc/localtime && \
    echo "America/Santiago" > /etc/timezone

# Install essential linux packages
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

# setup the directory where we will mount the codebase fron the host
RUN mkdir -p /app
WORKDIR /app

# Copy the Gemfile as well as the Gemfile.lock and install
# the RubyGems. This is a separate step so the dependencies
# will be cached unless changes to one of those two files
# are made.
COPY Gemfile Gemfile.lock ./
RUN gem install bundler -v 1.17.3
RUN bundle install

# copy the main application
COPY . ./

# Expose port 3000 to the Docker host, so we can access it
# from the outside.
EXPOSE 3000
